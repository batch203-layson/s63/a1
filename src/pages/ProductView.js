import { useState, useEffect, useContext } from "react";

import { Container, Card, Button, Row, Col } from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import Swal from "sweetalert2";

import UserContext from "../UserContext";

export default function ProductView() {

    const { user } = useContext(UserContext);

    // "useParams" hooks allows us to retrieve the courseId passed via the URL.

    const { productItemId } = useParams();

    const navigate = useNavigate();

    var quantity=1;

    function addition(){
        quantity+=1;
    }


    function subtract() {
        quantity-=1;
    }


    // Create state hooks to capture the information of a specific course and display it in our application.
    const [productItemName, setProductItemName] = useState('');
    const [productItemDescription, setProductItemDescription] = useState('');
    const [productItemPrice, setProductItemPrice] = useState(0);
    const [productItemStocks, setProductItemStocks] = useState(0);


    useEffect(() => {
        console.log(productItemId);

        fetch(`${process.env.REACT_APP_API_URL}/productitems/${productItemId}`)
            .then(res => res.json())
            .then(data => {

                console.log(data);

                setProductItemName(data.productItemName);
                setProductItemDescription(data.productItemDescription);
                setProductItemPrice(data.productItemPrice);
                setProductItemStocks(data.productItemStocks);

            });

    }, [productItemId])

    const checkout = (productItemId) => {

        fetch(`${process.env.REACT_APP_API_URL}/useraccounts/checkout`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                productItemId: productItemId
            })
        })
            .then(res => res.json())
            .then(data => {

                console.log(data);

                if (data) {
                    Swal.fire({
                        title: "Successfully Checkout Item",
                        icon: "success",
                        text: "You have successfully checkout this item."
                    });

                    navigate("/products");
                }
                else {
                    Swal.fire({
                        title: "Something went wrong",
                        icon: "error",
                        text: "Please try again."
                    });
                }

            });

    }

    return (
        <Container className="mt-5">
            <Row>
                <Col lg={{ span: 6, offset: 3 }}>
                    <Card>
                        <Card.Body className="text-center">
                            <Card.Title>{productItemName}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{productItemDescription}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>PhP {(productItemPrice*quantity)}</Card.Text>
                            <Card.Subtitle>Stocks:</Card.Subtitle>
                            <Card.Text>{productItemStocks}</Card.Text>
                            {
                                (user.id !== null)
                                    ?
                                    <>
                                        <Card.Subtitle>Quantity:</Card.Subtitle>
                                        <div className="d-flex align-items-center justify-content-center" style={{gap:".5rem"}}>

                                            
                                            <div>
                                                <span><Button variant="primary" size="lg" onClick={() => addition()}>+</Button></span>
                                                <span className="fs-3">
                                                    {quantity}
                                                </span>
                                                <span><Button variant="primary" size="lg" onClick={() => subtract()}>-</Button></span>
                                            </div>
                                            
                                        </div>
                                        <Button variant="primary" size="lg" onClick={() => checkout(productItemId)}>Checkout</Button>
                                       
                                    </>
                                    :
                                    <Button as={Link} to="/login" variant="success" size="lg">Login to Checkout Item</Button>
                            }
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    )
}
